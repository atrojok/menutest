sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/ui/core/Fragment"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, JSONModel, Fragment) {
        "use strict";

        return Controller.extend("at.menutest.controller.Main", {
            onInit: function () {

            },

            onMdrButton: function(ioEvent){
                //Simuliere Backend-Call für Ermittlung Einträge
                var laEntries = [],
                    loSelect = this.byId("selAnzahl"),
                    lsAnz = loSelect.getSelectedKey();
                
                switch (lsAnz) {
                    case "1":
                        laEntries = [{TEXT: "In MDR-SAP anzeigen", URL: "https://www.mdr.de"}];
                        break;

                    case "2":
                        laEntries = [{TEXT: "In MDR-SAP anzeigen", URL: "https://www.mdr.de"},
                                    {TEXT: "In VIS im Browser öffnen", URL: "https://www.xsuite.com"}];
                        break;

                    case "3":
                        laEntries = [{TEXT: "In MDR-SAP anzeigen", URL: "https://www.mdr.de",},
                                    {TEXT: "In VIS im Browser öffnen", URL: "https://www.xsuite.com"},
                                    {TEXT: "Irgendwo anders", URL: "https://www.sap.com"}];
                        break;
                
                    default:
                        return;

                }

                //Behandle je nach Anzahl der Einträge
                if (laEntries.length === 0) {
                    //Leer -> tu nix
                    return;

                } else if (laEntries.length === 1) {
                    //1 Eintrag -> direkt öffnen
                    var lsUrl = laEntries[0].URL;
                    if (lsUrl){window.open(lsUrl,"_blank");}

                } else  {
                    //Mehrere Einträge -> Als Menü öffnen
                    var loView = this.getView(),
                        loButton = ioEvent.getSource(),
                        loModel = new JSONModel(laEntries);

                    if (!this._oMenu) {
                        this._oMenu = Fragment.load({
                            name: "at.menutest.view.Menu",
                            controller: this
                        }).then(function(oMenu) {
                            this._oMenu = oMenu;
                            this._oMenu.setModel(loModel);
                            loView.addDependent(this._oMenu);
                            this._oMenu.openBy(loButton);
                            return this._oMenu;
                        }.bind(this));
                    } else {
                        this._oMenu.setModel(loModel);
                        this._oMenu.openBy(loButton);
                    }
                    
                }
                
            },

            onMenuItemSelected: function(ioEvent){
                var loMenuItem = ioEvent.getParameter("item"),
                    loMenu = loMenuItem.getParent(),
                    loModel = loMenu.getModel(),
                    lsPath = loMenuItem.getBindingContext().getPath(),
                    lsUrl = loModel.getProperty(lsPath + "/URL");
                
                if (lsUrl){
                    window.open(lsUrl,"_blank");
                }
            }
        });
    });
